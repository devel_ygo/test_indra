package com.indra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonaApirestApplication {

	public static void main(String[] args) {
		// System.setProperty("server.servlet.context-path", "/PruebaRest");
		SpringApplication.run(PersonaApirestApplication.class, args);
	}
}
