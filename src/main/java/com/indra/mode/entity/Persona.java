package com.indra.mode.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * CLase entidad, con la cual se mapea la tabla de la base de datos.
 * 
 * @author yefrigomez
 *
 */
@Entity
@Table(name = "Personas")
public class Persona {

	// nombres, apellidos, cedula, genero, edad
	@Id
	@Column(name = "cedula")
	private String cedula;

	@Column(name = "nombres")
	private String nombres;

	@Column(name = "apellidos")
	private String apellidos;

	@Column(name = "genero")
	private String genero;

	@Column(name = "edad")
	private byte edad;

	public Persona() {
		super();
	}

	public Persona(String cedula, String nombres, String apellidos, String genero, byte edad) {
		super();
		this.cedula = cedula;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.genero = genero;
		this.edad = edad;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public byte getEdad() {
		return edad;
	}

	public void setEdad(byte edad) {
		this.edad = edad;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.getCedula() + " " + this.getApellidos() + " " + this.getGenero() + " " + this.getEdad() + " "
				+ this.getGenero();
	}

}
