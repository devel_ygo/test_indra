package com.indra.generic;

import org.springframework.data.jpa.repository.JpaRepository;

import com.indra.mode.entity.Persona;
import org.springframework.stereotype.Repository;

@Repository
public interface IPersona extends JpaRepository<Persona, String> {

}
