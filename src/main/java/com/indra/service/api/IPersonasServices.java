package com.indra.service.api;

import java.util.List;

import com.indra.mode.entity.Persona;

/**
 * Interfaz con la cual definiremos los metodos principales del WS
 * 
 * @author yefrigomez
 *
 */
public interface IPersonasServices {

	public List<Persona> ListarPersonas();

	public Persona registrarPersonas(Persona persona);

	public Persona buscarPersonasId(String cedula);

	public Persona actualizarPersonas(Persona persona);

	public Persona borrarPersonasId(String cedula);

}
