package com.indra.service.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indra.generic.IPersona;
import com.indra.mode.entity.Persona;

/**
 * Clase transaccional con la cual realizaremos la implementación de los
 * comportamientos definidos para el WS.
 * 
 * @author yefrigomez
 *
 */
@Service
public class PersonaServicesImp implements IPersonasServices {

	// Instancia la clase implicitamente.
	@Autowired
	private IPersona iPersonas;

	@Override
	public List<Persona> ListarPersonas() {
		return iPersonas.findAll();
	}

	@Override
	public Persona registrarPersonas(Persona persona) {
		return iPersonas.save(persona);
	}

	@Override
	public Persona buscarPersonasId(String cedula) {
		return iPersonas.findById(cedula).get();
	}

	@Override
	public Persona actualizarPersonas(Persona personas) {
		return iPersonas.save(personas);
	}

	@Override
	public Persona borrarPersonasId(String cedula) {
		Persona per = this.buscarPersonasId(cedula);
		iPersonas.delete(per);
		return per;
	}

}
