package com.indra.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.indra.mode.entity.Persona;
import com.indra.service.api.PersonaServicesImp;

/**
 * Clase controladora de las peticiones realidas por el WS desde cualquier
 * cliente
 * 
 * @author yefrigomez
 *
 */
@RestController
@RequestMapping("/api")
public class PersonaController {

	@Autowired
	PersonaServicesImp servicesImp;

	/**
	 * Función con la cual consultaremos todos los registros de la tabla personas.
	 * 
	 * @return Estrucutra json con los datos del registro
	 */
	@GetMapping("/personas")
	public List<Persona> listarPersonas() {
		return servicesImp.ListarPersonas();
	}

	/**
	 * Función con la cual se pretende insertar un registro de Personas en la base
	 * de datos.
	 * 
	 * @param personas Objeto con el cual encapsulamos el registro que se inserta.
	 * @return Estrucutra json con los datos del registro.
	 */
	@PostMapping("/personas")
	@Transactional
	public Persona insertaPersonas(@RequestBody Persona personas) {
		return servicesImp.registrarPersonas(personas);
	}

	/**
	 * Funcion con la cual pretendemos consultar el registro de una persona cuando
	 * se envia la petición desde el servicio web.
	 * 
	 * @param cedula Identificador de la persona a quien se pretende buscar.
	 * @return Estrucutra json con los datos del registro.
	 */
	@GetMapping("/personas/{cedula}")
	public Persona buscarPersona(@PathVariable(name = "cedula") String cedula) {
		Persona per = servicesImp.buscarPersonasId(cedula);
		return per;
	}

	/**
	 * Funcion con la cual pretendemos actualizar el registro de una persona cuando
	 * se envia la petición desde el servicio web.
	 * 
	 * @param cedula   Identificador de la persona a quien se pretende actualizar.
	 * @param personas Objeto con el cual encapsulamos el registro que se actualiza.
	 * @return Estrucutra json con los datos del registro actualizado.
	 */
	@PutMapping("/personas/{cedula}")
	@Transactional
	public Persona actualizarPersonas(@PathVariable(name = "cedula") String cedula, @RequestBody Persona personas) {
		Persona perSel = new Persona();
		Persona perAct = new Persona();

		perSel = servicesImp.buscarPersonasId(cedula);
		perSel.setNombres(personas.getNombres());
		perSel.setApellidos(personas.getApellidos());
		perSel.setEdad(personas.getEdad());
		perSel.setGenero(personas.getGenero());

		perAct = servicesImp.actualizarPersonas(perSel);

		return perAct;
	}

	/**
	 * Funcion con la cual pretendemos eliminar el registro de una persona cuando se
	 * envia la petición desde el servicio web.
	 * 
	 * @param cedula   Identificador de la persona a quien se pretende eliminar.
	 * @param personas Objeto con el cual encapsulamos el registro que se elimina.
	 * @return Estrucutra json con los datos del registro eliminado.
	 */
	@DeleteMapping("/borrarPersonas/{cedula}")
	@Transactional
	public Persona elminarPersona(@PathVariable(name = "cedula") String cedula, @RequestBody Persona personas) {
		Persona p = servicesImp.borrarPersonasId(cedula);
		personas = p;
		return personas;
	}

}
