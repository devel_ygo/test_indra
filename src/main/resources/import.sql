DROP TABLE IF EXISTS personas;
CREATE TABLE personas (cedula VARCHAR(22) NOT NULL PRIMARY KEY,nombres VARCHAR(30) NOT NULL,apellidos VARCHAR(30) NOT NULL,genero VARCHAR(2) NOT NULL,edad INT NOT NULL);
/* Populate tabla personas */
INSERT INTO personas (cedula, nombres, apellidos, genero, edad) VALUES('10155584', 'Andrés', 'Guzmán','M','26');
INSERT INTO personas (cedula, nombres, apellidos, genero, edad) VALUES('10155345584', 'Mr. John', 'Doe','M','26');
INSERT INTO personas (cedula, nombres, apellidos, genero, edad) VALUES('101234584', 'Linus', 'Torvalds','M','26');
INSERT INTO personas (cedula, nombres, apellidos, genero, edad) VALUES('456334', 'Rasmus', 'Lerdorf','M','26');
INSERT INTO personas (cedula, nombres, apellidos, genero, edad) VALUES('45436', 'Erich', 'Gamma','M','26');